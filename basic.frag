#version 330 core

out vec4 color;

in Vertex { smooth vec4 color; smooth vec4 normal; smooth vec4 camera; smooth vec2 uv; } vertex;

void main(void) {
	color = vertex.color;
//    color = vec4(1,1,1,1);
}
