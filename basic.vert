#version 330 core

layout(location = 0) in vec4 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 uv;

uniform vec4 color;
uniform mat4 p_matrix, mv_matrix;

out Vertex {
	vec4 color;
	vec4 normal;
	vec4 camera;
    vec2 uv;
} vertex;

void main(void){
	vertex.camera = mv_matrix * position;
	vertex.normal = mv_matrix * vec4(normal, 0.0);
//	vertex.color = vec4(1.f,0.3f,0.1f,1);
    vertex.color = color;
    vertex.uv = uv;
    
    gl_Position = p_matrix * vertex.camera;
}
