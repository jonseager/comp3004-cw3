#include "utils.h"
#include "camera.h"
int pnpoly(int nvert, float *vertx, float *verty, float testx, float testy);
GLboolean checkCollisions(GLfloat x, GLfloat y, GLfloat z);

void Camera::yawLeft(GLfloat angle) {
    rot_angle -= angle;
    // Bound the angle to 0 < angle < 360
    rot_angle = (rot_angle > 359.9) ? 0 : rot_angle;
    rot_angle = (rot_angle < -359.9) ? 0 : rot_angle;
    // Do the rotation!
    *view = translate(*view, vec3(-x_movement, -y_movement, -z_movement));
    *view = rotate(*view, -angle, vec3(0, 0.0f, 1.0f));
    *view = translate(*view, vec3(x_movement, y_movement, z_movement));
}

void Camera::yawRight(GLfloat angle) {
    rot_angle += angle;
    // Bound the angle to 0 < angle < 360
    rot_angle = (rot_angle > 359.9) ? 0 : rot_angle;
    rot_angle = (rot_angle < -359.9) ? 0 : rot_angle;
    // Do the rotation!
    *view = translate(*view, vec3(-x_movement, -y_movement, -z_movement));
    *view = rotate(*view, angle, vec3(0, 0.0f, 1.0f));
    *view = translate(*view, vec3(x_movement, y_movement, z_movement));
}

void Camera::pitchUp(GLfloat angle) {
    pitch += angle;
    // Translate back to origin
    *view = translate(*view, vec3(-x_movement, -y_movement, -z_movement));
    // Rotate back to zero
    *view = rotate(*view, -rot_angle, vec3(0,0,1));
    // Perform pitch rotation
    *view = rotate(*view, -angle, vec3(1,0,0));
    // Re-apply yaw
    *view = rotate(*view, rot_angle, vec3(0,0,1));
    // Re-apply movement
    *view = translate(*view, vec3(x_movement, y_movement, z_movement));
}

void Camera::pitchDown(GLfloat angle) {
    pitch -= angle;
    // Translate back to origin
    *view = translate(*view, vec3(-x_movement, -y_movement, -z_movement));
    // Rotate back to zero
    *view = rotate(*view, -rot_angle, vec3(0,0,1));
    // Perform pitch rotation
    *view = rotate(*view, angle, vec3(1,0,0));
    // Re-apply yaw
    *view = rotate(*view, rot_angle, vec3(0,0,1));
    // Re-apply movement
    *view = translate(*view, vec3(x_movement, y_movement, z_movement));
}

void Camera::forward(GLfloat distance) {
    // Calculate forward component along x/y axes
    GLfloat x_diff = distance * sin((rot_angle/180.0f)*PI);
    GLfloat y_diff = distance * -cos((rot_angle/180.0f)*PI);
    // Check the camera won't collide with anything
    if (checkCollisions(x_movement-x_diff,y_movement+y_diff,z_movement)) {
        // Add to total movement
        y_movement += y_diff;
        x_movement -= x_diff;
        // Move the camera!
        *view = translate(*view, vec3(-x_diff, y_diff, 0.0f));
    }
}

void Camera::backward(GLfloat distance) {
    // Calculate backward component along x/y axes
    GLfloat x_diff = distance * sin((rot_angle/180.0f)*PI);
    GLfloat y_diff = distance * -cos((rot_angle/180.0f)*PI);
    // Check the camera won't collide with anything
    if (checkCollisions(x_movement+x_diff,y_movement-y_diff,z_movement)) {
        // Add to total movement
        y_movement -= y_diff;
        x_movement += x_diff;
        // Move the camera!
        *view = translate(*view, vec3(x_diff, -y_diff, 0.0f));
    }
}

void Camera::left(GLfloat distance) {
    yawLeft(90.f);
    forward(distance);
    yawRight(90.f);
}

void Camera::right(GLfloat distance) {
    yawRight(90.f);
    forward(distance);
    yawLeft(90.f);
}

void Camera::elevationUp(GLfloat distance) {    
    z_movement -= distance;
    *view = translate(*view, vec3(0, 0, -distance));
}

void Camera::elevationDown(GLfloat distance) {
    if (z_movement + distance < 0) {
        z_movement += distance;
        *view = translate(*view, vec3(0, 0, distance));
    }
}

void Camera::report() {
    printf("%f %f %f %f %f\n", x_movement, y_movement, z_movement, rot_angle, pitch);
}
void Camera::reset() {
    *view = lookAt(vec3(0,0,3), vec3(0,5,2), vec3(0.0f,1.0f,0.0f));
    x_movement = 0.0f;
    y_movement = 0.0f;
    z_movement = 0.0f;
    rot_angle = 0.0f;
    pitch = 0.0f;
}
void Camera::position3() {
    reset();
    x_movement = -19.619055;
    y_movement = -132.041794;
    z_movement = -14.900021;
    rot_angle = 182.0f;
    pitch = 0;
    update();
}
void Camera::position2() {
    reset();
    x_movement = -5.024060;
    y_movement = 88.114769;
    z_movement = -14.900021;
    rot_angle = 6.f;
    pitch = 0;
    update();
}
void Camera::position1() {
    reset();
    x_movement = 63.595051;
    y_movement = 92.989639;
    z_movement = -36.500015;
    rot_angle = 32.f;
    pitch = 0;
    update();
}
void Camera::update() {
    *view = glm::rotate(*view, -pitch, vec3(1,0,0));
    *view = glm::rotate(*view, rot_angle, vec3(0,0,1));
    *view = glm::translate(*view, vec3(x_movement,y_movement,z_movement));
}
void Camera::update(GLfloat x, GLfloat y, GLfloat z, GLfloat yaw, GLfloat npitch) {
    x_movement = x;
    y_movement = y;
    z_movement = z;
    rot_angle = yaw;
    pitch = npitch;
    update();
}

GLboolean checkCollisions(GLfloat x, GLfloat y, GLfloat z) {
    //check if inside shuttle
    if (pow((x-(-5)),2) + pow((y-(-5)),2) < pow(3,2) && ((z < 0 && z > -5.f) || z == 0)) {
        return false;
    }
    // Bottom part of station
    float xco1[4] = {-32.2f, -58.7f, -33.f, -6.5f};
    float yco1[4] = {-55.2f, -29, -3.f, -29.5f};
    if (pnpoly(4, xco1, yco1, x, y) > 0 && ((z < 0 && z > -2.0f) || z == 0)) { return false;}
    // Top part of station
    float xco2[4] = {-22.1, -22.1f, -42.9f, -42.9f};
    float yco2[4] = {-18.8f, -39.3, -39.3f, -18.8f};
    if (pnpoly(4, xco2, yco2, x, y) > 0 && ((z < -2.0f && z > -9.2f) || z == 0)) { return false;}
    // Antennae
    float xco3[4] = {-7.1, -13.0f, -13.0f, -7.1f};
    float yco3[4] = {7.4f, 7.4, 12.7f, 12.7f};
    if (pnpoly(4, xco3, yco3, x, y) > 0 && ((z < 0.0f && z > -4.9f) || z == 0)) { return false;}
    // Planet
    float xco4[4] = {34.1f, 45, 45.f, 34.1f};
    float yco4[4] = {40.1, 40.1, 50.f, 50.f};
    if (pnpoly(4, xco4, yco4, x, y) > 0 && ((z < -34.1f && z > -43.1f))) { return false;}
    return true;
}

// Taken from http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html
int pnpoly(int nvert, float *vertx, float *verty, float testx, float testy) {
    int i, j, c = 0;
    for (i = 0, j = nvert-1; i < nvert; j = i++) {
        if ( ((verty[i]>testy) != (verty[j]>testy)) &&
            (testx < (vertx[j]-vertx[i]) * (testy-verty[i]) / (verty[j]-verty[i]) + vertx[i]) )
            c = !c;
    }
    return c;
}




