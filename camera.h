#include "utils.h"
#ifndef __COMP3004__camera__
#define __COMP3004__camera__

class Camera {
    
private:
    mat4 * view;
    GLfloat rot_angle, pitch;
    GLfloat x_movement;
    GLfloat y_movement;
    GLfloat z_movement;
    
public:
    Camera(mat4 *View) {
        x_movement = 0.0f;
        y_movement = 0.0f;
        z_movement = 0.0f;
        rot_angle = 0.0f;
        pitch = 0.0f;
        view = View;
    }
    
    void yawLeft(GLfloat angle = 1.0f);
    void yawRight(GLfloat angle = 1.0f);
    
    void pitchUp(GLfloat angle = 1.0f);
    void pitchDown(GLfloat angle = 1.0f);
    
    void forward(GLfloat distance = 0.5f);
    void backward(GLfloat distance = 0.5f);
    void left(GLfloat distance = 0.5f);
    void right(GLfloat distance = 0.5f);
    
    void elevationUp(GLfloat distance = 0.1f);
    void elevationDown(GLfloat distance = 0.1f);
    
    void update();
    void update(GLfloat x, GLfloat y, GLfloat z, GLfloat yaw, GLfloat npitch);
    void report();
    void reset();
    
    void position1();
    void position2();
    void position3();
};

#endif 
