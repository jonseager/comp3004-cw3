#include "utils.h"
#include "camera.h"
#include "model.h"
#include "main.h"

// 45 deg Field of View, Aspect Ration, Near Clip, Far Clip
mat4 projection = perspective(45.0f, 1.0f, 1.0f, 300.0f);
mat4 view = lookAt(vec3(0,0,3), vec3(0,5,2), vec3(0.0f,1.0f,0.0f));
Camera c (&view);
FILE * file;
GLboolean tour = false;

std::vector<Model*> Models;
GLfloat forwardSpeed = 0.0f;

int main(void) {
	setupGL();
    
    GLuint texture = 0;
    Model terrain((char*)"new_terrain3.obj", (char*)"rocks.tga", texture++, true, (char*)"texture.frag", &view, &projection); Models.push_back(&terrain);
    Model moon((char*)"sphere.obj", (char*)"moon.tga", texture++, true, (char*)"texture.frag", &view, &projection); Models.push_back(&moon);
    Model shuttle((char*)"shuttle2.obj", (char*)"shuttle.tga",texture++, true, (char*)"texture.frag", &view, &projection); Models.push_back(&shuttle);
    Model anten((char*)"antennae.obj", (char*)"anten.tga", texture++, true, (char*)"texture.frag", &view, &projection); Models.push_back(&anten);
    Model planet((char*)"planet-middle.obj", (char*)"saturnmap.tga", texture++, true, (char*)"texture.frag", &view, &projection); Models.push_back(&planet);
    Model rings((char*)"planet-rings.obj", (char*)"rings.tga", texture++, true, (char*)"texture.frag", &view, &projection); Models.push_back(&rings);
    Model flag((char*)"flag.obj", (char*)"flag.tga", texture++, true, (char*)"texture.frag", &view, &projection); Models.push_back(&flag);
    Model station((char*)"station1.obj", (char*)"station.tga", texture++, true, (char*)"texture.frag", &view, &projection); Models.push_back(&station);
    Model floater((char*)"floater.obj", (char*)"floater.tga", texture++, false, (char*)"texture.frag", &view, &projection); Models.push_back(&floater);
    
    shuttle.translate(vec3(-5,-5,0));
    anten.translate(vec3(-10,10,0.3f));
    anten.setColor(vec4(0.7f,0.7f,0.7f,1.0f));
    station.rotate(45.f, vec3(0,0,1));
    station.translate(vec3(-32.5f,-29.f,-1.f));
    planet.translate(vec3(40,45,-40.f));
    rings.translate(vec3(40,45,-40.f));
    rings.rotate(70.0, vec3(1,0,0));
    flag.rotate(90.f, vec3(0,0,1));
    flag.translate(vec3(-32.5f,-29.f,-10.f));

    GLfloat pl_angle = 0.0f,angle = 0.0f, moon_angle = 0.0f, floater_height = -1.0f, floater_mult = -1.0f, newx, newy, newz, newrot, newpitch, time = 0.0f;
    
    while(true) {
        // Clear the screen
        glClearColor(0.0, 0.0, 0.0, 1.0); glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
        // Render the models
        for(int i = 0; i < Models.size(); i++) Models[i]->render();
        // Moon orbiting the planet
        moon_angle-=0.003;
        angle += 0.5f;
        moon.translate(vec3((10*cos(moon_angle) + 40),(10*sin(moon_angle) + 45),-40.0f));
        moon.rotate(angle, vec3(0,0,1));
        // Lunar lander thing
        floater_mult = (floater_height < -5.0f) ? 1.f : floater_mult;
        floater_mult = (floater_height > -1.0f) ? -1.f : floater_mult;
        floater_height += (floater_mult * 0.01f);
        floater.translate(vec3((5*cos(moon_angle) + 10),(5*sin(moon_angle) + 10),floater_height));
        floater.rotate(pl_angle, vec3(0,0,1));
        // Spin the radar dishes
        anten.rotate(angle, vec3(0,0,1));
        planet.rotate(pl_angle, vec3(0,0.0,1.0));
        pl_angle += 0.4f;
        
        // Walk forward and adjust to use controls
        if (!tour) {
            c.forward(forwardSpeed);
        }
        adjustCamera(&c);

        // Tour stuff
        if (tour && glfwGetTime() > time+0.005f) {
            int matches = fscanf(file, "%f %f %f %f %f\n", &newx, &newy, &newz, &newrot, &newpitch );
            if (matches == 5) {
                time = glfwGetTime();
                c.reset();
                c.update(newx,newy,newz,newrot,newpitch);
            }
            else {
                tour = false;
                c.reset();
            }
            
        }
        glfwSwapBuffers();
        
	}
}

// Helper methods
void setupGL() {
    if (!glfwInit()) exit(EXIT_FAILURE);
    // Open a GLFW Window
	glfwOpenWindowHint(GLFW_FSAA_SAMPLES, 4);
	glfwOpenWindowHint(GLFW_OPENGL_VERSION_MAJOR, 3);
	glfwOpenWindowHint(GLFW_OPENGL_VERSION_MINOR, 2);
	glfwOpenWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	
    if (!glfwOpenWindow(800, 800, 0, 0, 0, 0, 24, 0, GLFW_WINDOW)) {
		glfwTerminate();
		exit(EXIT_FAILURE);
	}
	glfwSetWindowTitle("Jon Seager - js4g10 - Coursework 3");
    // Keyboard
    glfwSetKeyCallback((GLFWkeyfun)positions);
    // Makes glew play nice on a Mac
    glewExperimental = GL_TRUE;
	glewInit();
	glGetError();
    // Fill polygons, not wireframe
    glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
    // Enable depth test
    glDepthMask(GL_TRUE);
    glDepthFunc(GL_LEQUAL);
    glDepthRange(0.0f, 1.0f);
    glEnable(GL_DEPTH_TEST);
	// Cull triangles which normal is not towards the camera
    glCullFace(GL_BACK);
    glEnable(GL_CULL_FACE);
}

void adjustCamera(Camera * c) {
    if (!tour) {
        if (glfwGetKey(GLFW_KEY_LEFT)) { c->yawLeft(0.5f); }
        else if (glfwGetKey(GLFW_KEY_RIGHT)) { c->yawRight(0.5f); }
        else if (glfwGetKey('I') || glfwGetKey(GLFW_KEY_PAGEUP)) { c->elevationUp(0.5f);   }
        else if (glfwGetKey('K') || glfwGetKey(GLFW_KEY_PAGEDOWN)) { c->elevationDown(0.5f); }
    }
}

void positions(int key, int action) {
    if(!tour) {
        if (glfwGetKey('P')) { c.position1(); }
        else if (glfwGetKey('R')) { c.reset(); }
        else if (glfwGetKey('Y')) { c.position2(); }
        else if (glfwGetKey('U')) { c.position3(); }
        else if (glfwGetKey(GLFW_KEY_SPACE)) { forwardSpeed = 0; }
        else if (glfwGetKey(GLFW_KEY_UP)) { forwardSpeed += 0.1f; }
        else if (glfwGetKey(GLFW_KEY_DOWN)) {
            forwardSpeed -= 0.1f;
            forwardSpeed = (forwardSpeed < 0) ? 0 : forwardSpeed;
        }
        else if (glfwGetKey('T')) {
            printf("Starting Tour...\n");
            tour = true;
            file = fopen("tour.txt", "r");
            if (file == NULL) {
                printf("Couldn't open tour file\n");
                tour = false;
            }
        }
        else if (glfwGetKey('H')) {
            std::cout << "Controls:\n----------\nUp Arrow: increase forward speed\nDown Arrow: decrease forward speed\nLeft Arrow: yaw left\nRight Arrow: yaw right\nPage Up/I: Elevation up\nPage Down/K: Elevation down\nSpace: brake!\nT: Begin tour\nE: End tour\nQ/ESC: Quit :(\n";
        }
    }
    else {
        if (glfwGetKey('E')) {
            tour = false;
            c.reset();
        }
    }
    if (glfwGetKey(GLFW_KEY_ESC) || glfwGetKey('Q')) {
        glfwTerminate();
        exit(EXIT_SUCCESS);
    }
}
