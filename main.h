#ifndef COMP3004_main_h
#define COMP3004_main_h
int main(void);
void adjustCamera(Camera * c);
void setupGL();
void render(GLuint shader, std::vector<vec3> vertices, mat4 Model, mat4 View, mat4 Projection);
void setupGeom(std::vector<vec3> vertices, std::vector<vec3> normals, std::vector<vec2> uvs, GLuint shader);
void positions(int key, int action);
#endif
