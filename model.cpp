#include "utils.h"
#include "model.h"

void Model::loadGeometry() {
	// Generate vertex buffer objects and array objects
    glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	
    glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(vec3), &vertices[0], GL_STATIC_DRAW);
    
    glGenBuffers(1, &normalsbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, normalsbuffer);
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(vec3), &normals[0], GL_STATIC_DRAW);
    
    glGenBuffers(1, &uvbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
	glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(vec2), &uvs[0], GL_STATIC_DRAW);
    
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
    
    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, normalsbuffer);
    glVertexAttribPointer((GLuint)1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	
    glEnableVertexAttribArray(2);
    glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
    glVertexAttribPointer((GLuint)2, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
}

void Model::loadTexture() {
    loadTexture(texture_file);
    GLuint texture = glGetUniformLocation(shader, "texture_sampler");
    glUniform1i(texture, textureOffset);
}

// Load a texture file from disk
GLuint Model::loadTexture(char* text) {

    // Create one OpenGL texture
    glGenTextures(1, &textureID);    
    // Set the active texture unit
    glActiveTexture(GL_TEXTURE0 + textureOffset);
    
    // Bind the newly created texture
    glBindTexture(GL_TEXTURE_2D, textureID);
    // Read the texture from file
    glfwLoadTexture2D(text, textureOffset);
    
    // Filtering stuff
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glGenerateMipmap(GL_TEXTURE_2D);
    // Return the texture id
    return textureID;
}

void Model::loadShader() {
    shader = setupShaders((char*)"basic.vert", frag_file);
    glUseProgram(shader);
}

void Model::render() {
    // Use the right texture
    glActiveTexture(GL_TEXTURE0 + textureOffset);
    glBindTexture(GL_TEXTURE_2D, this->textureID);
    // Load the correct vertex array object for the model
    glBindVertexArray(vao);
    // Load the correct shader fo the model
    glUseProgram(shader);
    // Translate the model matrix
    model = glm::rotate(model, -rotationAngle, rotationAxis);
    *view = glm::translate(*view, -translation);
    mat4 MV = *view * model;
    // Bind mv and p matrices into shader
    glUniform4f(glGetUniformLocation(shader, "color"), color[0], color[1], color[2], color[3]);
    glUniformMatrix4fv(glGetUniformLocation(shader, "mv_matrix"), 1, GL_FALSE, value_ptr(MV));
    glUniformMatrix4fv(glGetUniformLocation(shader, "p_matrix"), 1, GL_FALSE, value_ptr(*projection));
    // Draw the vertices passed in
	glDrawArrays(GL_TRIANGLES, 0, (int)vertices.size());
    // Translate model matrix back
    *view = glm::translate(*view, translation);
    model = glm::rotate(model, rotationAngle, rotationAxis);
}

void Model::scale(float n) {
    this->model = glm::scale(this->model, vec3(n,n,n));
}

void Model::translate(vec3 translation) {
    this->translation = translation;
}

void Model::rotate(GLfloat angle, vec3 axis){
    this->rotationAngle = angle;
    this->rotationAxis = axis;
}

void Model::setColor(vec4 color) {
    this->color = color;
}

// Inspired by http://www.opengl-tutorial.org/beginners-tutorials/tutorial-7-model-loading/
bool Model::loadOBJ(const char * path, std::vector<glm::vec3> & out_vertices, std::vector<glm::vec2> & out_uvs, std::vector<glm::vec3> & out_normals,GLboolean flipUV){
    std::vector<int> v_index, uv_index, n_index;
    std::vector<glm::vec3> temp_vertices, temp_normals;
    std::vector<glm::vec2> temp_uvs;
    
    FILE * file = fopen(path, "r");
    if (file == NULL) return false;
    
    while (true) {
        char coordType[128];
        if (fscanf(file, "%s", coordType) == EOF) break;
        if (strcmp(coordType, "v") == 0 ){
            glm::vec3 vertex;
            fscanf(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z );
            temp_vertices.push_back(vertex);
        }
        else if (strcmp(coordType, "vt") == 0) {
            glm::vec2 uv;
            fscanf(file, "%f %f\n", &uv.x, &uv.y );
            uv.y = flipUV ? -uv.y : uv.y;
            temp_uvs.push_back(uv);
        }
        else if (strcmp(coordType, "vn") == 0) {
            glm::vec3 normal;
            fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z );
            temp_normals.push_back(normal);
        }
        else if (strcmp(coordType, "f") == 0) {
            std::string vertex1, vertex2, vertex3;
            unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
            int matches = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n",&vertexIndex[0],&uvIndex[0],&normalIndex[0],&vertexIndex[1],&uvIndex[1],&normalIndex[1],&vertexIndex[2],&uvIndex[2],&normalIndex[2] );
            if (matches != 9){
                printf("Incorrect formatting in file %s", path);
                return false;
            }
            v_index.push_back(vertexIndex[0]);
            v_index.push_back(vertexIndex[1]);
            v_index.push_back(vertexIndex[2]);
            uv_index.push_back(uvIndex[0]);
            uv_index.push_back(uvIndex[1]);
            uv_index.push_back(uvIndex[2]);
            n_index.push_back(normalIndex[0]);
            n_index.push_back(normalIndex[1]);
            n_index.push_back(normalIndex[2]);
        }
        else {
            char junk[1500];
            fgets(junk, 1500, file);
        }
    }
    
    // For each vertex of each triangle
    for(int i=0; i<v_index.size(); i++){
        int vertexIndex = v_index[i], uvIndex = uv_index[i], normalIndex = n_index[i];
        glm::vec3 vertex = temp_vertices[ vertexIndex-1 ], normal = temp_normals[ normalIndex-1 ];
        out_vertices.push_back(vertex);
        out_normals.push_back(normal);
        glm::vec2 uv = temp_uvs[ uvIndex-1 ];
        out_uvs.push_back(uv);
    }
    return true;
}




