#include "utils.h"
#include "camera.h"
#ifndef __COMP3004__model__
#define __COMP3004__model__

class Model {
    
private:
    char* model_file;
    char* texture_file;
    char* frag_file;
    
    std::vector<vec3> vertices, normals;
    std::vector<vec2> uvs;
    
    GLuint shader;
    GLuint vao, vertexbuffer, normalsbuffer, uvbuffer;
    GLuint textureOffset, textureID;
    vec4 color;
    
    vec3 translation, rotationAxis;
    GLfloat rotationAngle;
    
    mat4 *view, *projection;
    mat4 model;
    
    bool loadOBJ(const char * path,std::vector<glm::vec3> & out_vertices,std::vector<glm::vec2> & out_uvs,std::vector<glm::vec3> & out_normals,GLboolean flipUV);
    GLuint loadTexture(char* text);
    
    void loadGeometry();
    void loadTexture();
    void loadShader();
    
public:
    Model(char* file, char* texture, GLuint textureOffset, GLboolean flipUV,  char* frag_shader, mat4* view, mat4* projection) {
        model_file = file;
        texture_file = texture;
        this->textureOffset = textureOffset;
        frag_file = frag_shader;
        this->model = mat4(1.f);
        this->view = view;
        this->projection = projection;
        loadOBJ(model_file, vertices, uvs, normals,flipUV);
        this->color = vec4(0.7f,0.7f,0.7f,1);
        translation = vec3(0,0,0);
        rotationAxis = vec3(0.0f,0.0f,1.0f);
        rotationAngle = 0.0f;
        loadGeometry();
        loadShader();
        loadTexture();
    }
    
    Model(char* file, char* frag_shader, mat4* view, mat4* projection) {
        model_file = file;
        frag_file = frag_shader;
        this->model = mat4(1.f);
        this->view = view;
        this->projection = projection;
        loadOBJ(model_file, vertices, uvs, normals, false);
        this->color = vec4(0.7f,0.7f,0.7f,1);
        translation = vec3(0,0,0);
        rotationAxis = vec3(0.0f,0.0f,1.0f);
        rotationAngle = 0.0f;
        loadGeometry();
        loadShader();
    }
    
    void scale(float n);
    void translate(vec3 translation);
    void rotate(GLfloat angle, vec3 axis);
    void setColor(vec4 color);
    void render();
};

#endif