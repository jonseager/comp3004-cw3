Notes:
------
- I used a Mac!
- Mouse is disabled

I started by building a basic 'framework'. The Camera class contains all the methods needed for a first person camera. This could be plugged into any OpenGL Application.

I then built a Model class. This is passed a model and texture when constructed and contains the Vertex Buffer Object and shader along with all other info needed to render that model. It can be moved around the scene with Model.translate(), Model.rotate(), Model.scale(). When Model.render() is called, the model class switches the current VBO/texture unit/shader etc automatically before drawing.

Once these components were done, I made some basic models in Blender and used my 'framework' to place them on the scene, using basic iteration for animation.

Project can be built on Mac OSX easily by opening the XCode Project and clicking Run!


Controls
---------
UP ARROW: Increase forward speed (each individual press, not continuous)
DOWN ARROW: Decrease forward speed (each individual press)
LEFT ARROW: Look left (Yaw left)
RIGHT ARROW: Look right (Yaw right)
PAGE UP/I: Move camera up (extra key because my Mac has no PAGE UP!)
PAGE DOWN/K: Move camera down
T: Start tour (Resets to start point when finished)
E: Exit tour
H: Print Help to stdout
P: Viewpoint 1
Y: Viewpoint 2
U: Viewpoint 3
R: Reset camera to start point

Files:
-------
COMP3004.xcodeproj/  - XCode Project Directory
screenshot.jpg       - Screenshot

*.tga       - Texture files
*.obj       - Model files exported from Blender

tour.txt    - contains list of positions for tour

utils.h     - includes glm/opengl/glfw, defines PI and helper methods
utils.cpp   - shader loader, check function

camera.h    - header file for camera controls
camera.cpp  - contains yaw/pitch/elevation/movement methods for camera

model.h     - header file for model class
model.cpp   - provide functions for loading/rendering models

main.h      - header file for main.cpp
main.cpp    - contains main program commands and loop/keyboard listener

basic.vert    - vertex shader
texture.frag  - fragment shader