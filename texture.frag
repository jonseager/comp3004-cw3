#version 330 core

out vec4 color;

uniform mat4 p_matrix, mv_matrix;
uniform sampler2D texture_sampler;

in Vertex { smooth vec4 color; smooth vec4 normal; smooth vec4 camera; smooth vec2 uv; } vertex;


const vec4 direction    = vec4(0.0, 1.0, 0.0, 0.0); // Light shining from positive side of Z-axis, light at infinity (w = 0)
const vec4 diffuse      = vec4(0.8, 0.8, 0.8, 0.0);
const vec4 ambient      = vec4(0.5, 0.5, 0.5, 0.0);
const vec4 specular     = vec4(1.0, 1.0, 1.0, 0.0);


void main(void) {
	color = vec4(texture(texture_sampler, vertex.uv).rgb, 1.0);
    
    vec3 mv_direction   = (mv_matrix * direction).xyz;
//    vec3 mv_direction   = (direction).xyz;
    vec3 normal         = normalize(vertex.normal.xyz);
    vec3 camera         = normalize(vertex.camera.xyz);
    vec3 reflection     = reflect(-mv_direction, normal);
    // Phong algorithm
    vec4 frag_diffuse           = vertex.color;
    vec4 diffuse_factor         = max(-dot(normal, mv_direction), 0.0) * diffuse;
    vec4 ambient_diffuse_factor = diffuse_factor + ambient;
    vec4 specular_factor        = max(pow(-dot(reflection, camera), 30.0), 0.0) * specular;
    
    color *= (specular_factor * 0.1) + (ambient_diffuse_factor * frag_diffuse);
}