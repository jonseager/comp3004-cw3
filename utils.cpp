#include "utils.h"

// Stolen from tutorial
char* fileToBuffer(char* file) {
	FILE *fptr;
	long length;
	char *buf;
	fptr = fopen(file, "rb");
	if (!fptr) {
		fprintf(stderr, "failed to open %s\n", file);
		return NULL;
	}
	fseek(fptr, 0, SEEK_END);
	length = ftell(fptr);
	buf = (char *)malloc(length + 1);
	fseek(fptr, 0, SEEK_SET);
	fread(buf, length, 1, fptr);
	fclose(fptr);
	buf[length] = 0;
	return buf;
}

// Setup a shader without geometry shader
GLuint setupShaders(char* vert, char* frag) {
    GLchar *vertexsource, *fragmentsource;
    GLuint vertexshader, fragmentshader;
    GLuint shader;
    
	char error[1000];
	GLint Result = GL_FALSE;
    int InfoLogLength;
	
	shader = glCreateProgram();
	vertexsource = fileToBuffer((char *)vert);
	fragmentsource = fileToBuffer((char *)frag);
	
	vertexshader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexshader, 1, (const char **)&vertexsource, NULL);
	glCompileShader(vertexshader);
	
	// Check Vertex Shader
    glGetShaderiv(vertexshader, GL_COMPILE_STATUS, &Result);
    glGetShaderiv(vertexshader, GL_INFO_LOG_LENGTH, &InfoLogLength);
    glGetShaderInfoLog(vertexshader, InfoLogLength, NULL, error);
	if (InfoLogLength > 0) {
		fprintf(stdout, "%s\n", error);
	}
	
	fragmentshader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentshader, 1, (const char **)&fragmentsource , NULL);
    glCompileShader(fragmentshader);
	
    // Check Fragment Shader
    glGetShaderiv(fragmentshader, GL_COMPILE_STATUS, &Result);
    glGetShaderiv(fragmentshader, GL_INFO_LOG_LENGTH, &InfoLogLength);
    glGetShaderInfoLog(fragmentshader, InfoLogLength, NULL, error);
    if (InfoLogLength > 0) {
		fprintf(stdout, "%s\n", error);
	}
	
	glAttachShader(shader, vertexshader);
	glAttachShader(shader, fragmentshader);
	glLinkProgram(shader);
	
	glGetProgramiv(shader, GL_LINK_STATUS, &Result);
    glGetProgramiv(shader, GL_INFO_LOG_LENGTH, &InfoLogLength);
    glGetProgramInfoLog(shader, InfoLogLength, NULL, error);
	if (InfoLogLength > 0) {
		fprintf(stdout, "%s\n", error);
	}
    
    return shader;
}

void check(char *where) { // Function to check OpenGL error status
    char * what;
    int err = glGetError(); //0 means no error
    if(!err)
        return;
    if(err == GL_INVALID_ENUM)
        what = (char *)"GL_INVALID_ENUM";
    else if(err == GL_INVALID_VALUE)
        what = (char *)"GL_INVALID_VALUE";
    else if(err == GL_INVALID_OPERATION)
        what = (char *)"GL_INVALID_OPERATION";
    else if(err == GL_INVALID_FRAMEBUFFER_OPERATION)
        what = (char *)"GL_INVALID_FRAMEBUFFER_OPERATION";
    else if(err == GL_OUT_OF_MEMORY)
        what = (char *)"GL_OUT_OF_MEMORY";
    else
        what = (char *)"Unknown Error";
    fprintf(stderr, "Error (%d) %s  at %s\n", err, what, where);
    exit(1);
}
