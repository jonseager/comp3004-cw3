#ifndef __COMP3004__utils__
#define __COMP3004__utils__

    #include <iostream>
    #include <stdio.h>
    #include <stdlib.h>
    #include <stddef.h>
    #include <string.h>
    #include <vector>

    #include <glm/glm.hpp>
    #include <glm/gtc/matrix_transform.hpp>
    #include <glm/gtc/type_ptr.hpp>

    #include <GL/glew.h>
    #include <GL/glfw.h>

    using namespace glm;

    #define PI 3.14159265358979323846264338327950288

    char* fileToBuffer(char* file);
    GLuint setupShaders(char* vert, char* frag);
    void check(char *where);
#endif
